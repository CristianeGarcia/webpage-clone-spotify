Bootstrap project - Spotify clone

A project to study and apply Bootstrap concepts, that consists of a remake of the 2020's web page of Spotify.

This project is just a clone of the 2020’s webpage of Spotify, with the intention of study and apply the bootstrap concepts. 
*
* @copyright: all rights reserved to Spotify AB; imagines and layout conception from Spotify (www.spotify.com).
